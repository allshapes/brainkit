//
//  NSOrderedSet+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 28. 05. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import Foundation

extension NSOrderedSet {
	// swiftlint:disable empty_count
	public var isEmpty: Bool {
		self.count == 0
	}
	// swiftlint:enable empty_count
}
