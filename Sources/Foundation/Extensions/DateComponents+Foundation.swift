//
//  DateComponents+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 22. 02. 2021.
//  Copyright © 2021 Userbrain. All rights reserved.
//

import Foundation

extension DateComponents {
	public func toTimeString() -> String? {
		guard let hour = self.hour else { return nil }
		guard let minute = self.minute else { return nil }
		let second = self.second ?? 0
		let hours = String(format: "%02d", hour)
		let minutes = String(format: "%02d", minute)
		let seconds = String(format: "%02d", second)
		let value = [hours, minutes, seconds].joined(separator: ":")
		return value
	}

	public static func make(fromTimeString string: String) -> DateComponents? {
		let parts = string.components(separatedBy: ":")
		guard parts.count >= 2 else { return nil }
		guard let h = Int(parts[0]), h >= 0, h <= 23 else { return nil }
		guard let m = Int(parts[1]), m >= 0, m <= 59 else { return nil }
		var components = DateComponents(hour: h, minute: m)
		if parts.count > 2, let sec = Int(parts[2]), sec >= 0, sec <= 59 {
			components.second = sec
		}
		return components
	}
}
