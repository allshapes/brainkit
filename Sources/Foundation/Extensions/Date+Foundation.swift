//
//  Date+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 29. 04. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import Foundation

extension Date {
	public static func from(dateString: String) -> Date? {
		let dateParts = dateString.components(separatedBy: "-")
		guard dateParts.count == 3 else { return nil }
		var components = DateComponents()
		components.year = Int(dateParts[0])
		components.month = Int(dateParts[1])
		components.day = Int(dateParts[2])
		let date = Calendar.current.date(from: components)
		return date
	}

	public func isoDateString() -> String {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		let string = formatter.string(from: self)
		return string
	}

	public func isTimePast(hours: Int) -> Bool {
		let currentHours = Calendar.current.component(.hour, from: self)
		let currentMinutes = Calendar.current.component(.minute, from: self)
		let currentSecods = Calendar.current.component(.second, from: self)
		if currentHours > hours {
			return true
		}
		if currentHours == hours, currentMinutes > 0 || currentSecods > 0 {
			return true
		}
		return false
	}

	public var nextWeek: Date {
		let calendar = Calendar.current
		return calendar.date(byAdding: .weekOfYear, value: 1, to: self)!
	}

	public var previousWeek: Date {
		let calendar = Calendar.current
		return calendar.date(byAdding: .weekOfYear, value: -1, to: self)!
	}

	public var startOfWeek: Date {
		let calendar = Calendar.current
		let components = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
		let firstDay = calendar.date(from: components)!
		return firstDay
	}

	public var endOfWeek: Date {
		let calendar = Calendar.current
		return calendar.date(byAdding: .day, value: 6, to: self.startOfWeek)!
	}

	public var startOfMonth: Date {
		let calendar = Calendar.current
		let components = calendar.dateComponents([.year, .month], from: self)
		return calendar.date(from: components)!
	}

	public var endOfMonth: Date {
		var components = DateComponents()
		components.month = 1
		components.second = -1
		return Calendar.current.date(byAdding: components, to: startOfMonth)!
	}

	public func dayDistanceFrom(_ date: Date) -> Int {
		Calendar.current.dateComponents([.day], from: self, to: date).day ?? 0
	}

	public func monthDistanceFrom(_ date: Date) -> Int {
		Calendar.current.dateComponents([.month], from: self, to: date).month ?? 0
	}

	public func dateByAdding(days: Int) -> Date {
		Calendar.current.date(byAdding: .day, value: days, to: self) ?? self
	}

	public func dateByAdding(months: Int) -> Date {
		Calendar.current.date(byAdding: .month, value: months, to: self) ?? self
	}

	public func dayOrdinalityInEra() -> Int {
		Calendar.current.ordinality(of: .day, in: .era, for: self) ?? 0
	}
}
