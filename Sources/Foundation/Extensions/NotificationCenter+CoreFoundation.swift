//
//  NotificationCenter+CoreFoundation.swift
//  iOS
//
//  Created by Ondřej Hanák on 01.08.2022.
//

import Foundation

extension NotificationCenter {
	public func postOnMainThread(name: NSNotification.Name, object: Any?, userInfo: [AnyHashable: Any]? = nil) {
		DispatchQueue.main.async {
			self.post(name: name, object: object, userInfo: userInfo)
		}
	}
}
