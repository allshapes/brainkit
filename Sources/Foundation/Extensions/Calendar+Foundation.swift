//
//  Calendar+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 23.08.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import Foundation

// Based on https://sarunw.com/posts/getting-number-of-days-between-two-dates/

extension Calendar {
	public func numberOfDaysBetween(_ from: Date, and to: Date) -> Int {
		let fromDate = startOfDay(for: from)
		let toDate = startOfDay(for: to)
		let numberOfDays = dateComponents([.day], from: fromDate, to: toDate)
		return numberOfDays.day!
	}
}
