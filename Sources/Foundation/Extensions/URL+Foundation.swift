//
//  URL+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 10. 02. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import Foundation

extension URL {
	public func appendingParam(name: String, value: String?) -> URL {
		// based on https://stackoverflow.com/a/50990443/1548913
		guard var urlComponents = URLComponents(string: self.absoluteString) else { return self.absoluteURL }
		var queryItems: [URLQueryItem] = urlComponents.queryItems ?? []
		let paramItem = URLQueryItem(name: name, value: value)
		queryItems.append(paramItem)
		urlComponents.queryItems = queryItems
		return urlComponents.url ?? self
	}

	public init(staticString: StaticString) {
		guard let url = URL(string: "\(staticString)") else {
			fatalError("Invalid static URL string: \(staticString)")
		}
		self = url
	}
}

#if canImport(CryptoKit)
import CryptoKit

extension URL {
	// Based on https://stackoverflow.com/a/69462532/1548913
	@available(iOS 13.0, *)
	public func base64MD5Checksum() throws -> String {
		let bufferSize = 1024 * 1024

		let file = try FileHandle(forReadingFrom: self)
		defer {
			file.closeFile()
		}

		var md5 = CryptoKit.Insecure.MD5()

		// Read up to `bufferSize` bytes, until EOF is reached, and update MD5 context
		while autoreleasepool(invoking: {
			let data = file.readData(ofLength: bufferSize)
			if data.count > 0 {
				md5.update(data: data)
				return true // Continue
			} else {
				return false // End of file
			}
		}) { }

		let data = Data(md5.finalize())
		return data.base64EncodedString()
	}
}
#endif

#if canImport(UniformTypeIdentifiers)
import UniformTypeIdentifiers

extension URL {
	// Based on https://stackoverflow.com/a/66254547/1548913
	@available(iOS 14.0, *)
	public func mimeType() -> String {
		UTType(filenameExtension: pathExtension)?.preferredMIMEType ?? "application/octet-stream"
	}
}
#endif
