//
//  String+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 10. 02. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import Foundation

extension String {
	public static var thinSpace: String {
		"\u{2009}"
	}

	public static var nonBreakingSpace: String {
		"\u{00A0}"
	}

	public static var space: String {
		" "
	}

	public var data: Data {
		Data(self.utf8)
	}

	public func normalized() -> String {
		self.folding(options: .diacriticInsensitive, locale: .current).lowercased()
	}

	public func toBase64() -> String {
		self.data.base64EncodedString()
	}

	public func fromBase64() -> String? {
		guard let data = Data(base64Encoded: self) else { return nil }
		return data.string
	}

	public func removingNonBreakingSpaces() -> String {
		self.replacingOccurrences(of: String.nonBreakingSpace, with: " ")
	}

	public mutating func removeNonBreakingSpaces() {
		self = self.removingNonBreakingSpaces()
	}

	public func trimmingWhitespacesAndNewlines() -> String {
		self.trimmingCharacters(in: .whitespacesAndNewlines)
	}

	public mutating func trimWhitespacesAndNewlines() {
		self = self.trimmingWhitespacesAndNewlines()
	}

	public func capitalizingFirstLetter() -> String {
		return self.prefix(1).capitalized + self.dropFirst()
	}

	public mutating func capitalizeFirstLetter() {
		self = self.capitalizingFirstLetter()
	}

	public func lowercasingFirstLetter() -> String {
		return self.prefix(1).lowercased() + self.dropFirst()
	}

	public mutating func lowercaseFirstLetter() {
		self = self.lowercasingFirstLetter()
	}

	public func reversedDomain() -> String {
		return self.components(separatedBy: ".").reversed().joined(separator: ".")
	}

	public var isValidEmail: Bool {
		let pattern = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$" // from https://stackoverflow.com/a/201336/1548913
		let regex = try! NSRegularExpression(pattern: pattern, options: [])
		let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
		return matches.count == 1
	}

	public func containsWhitespaceOnly() -> Bool {
		let pattern = "^\\s+$"
		let predicate = NSPredicate(format: "SELF MATCHES %@", pattern)
		return predicate.evaluate(with: self)
	}

	/// Can be converted to Int without information loss?
	public var isInt: Bool {
		Int(self) != nil
	}

	/// Can be converted to Double without information loss?
	public var isDouble: Bool {
		Double(self) != nil
	}

	/// Consists of 0-9 digits?
	public var isDigits: Bool {
		CharacterSet(charactersIn: self).isSubset(of: CharacterSet(charactersIn: "0123456789"))
	}

	public func versionComparison(with otherVersion: String) -> ComparisonResult {
		let kSeparator = "."
		let kPaddingValue = "0"

		// Make sure strings have same amount of components
		var lhsComponents = self.components(separatedBy: kSeparator)
		var rhsComponents = otherVersion.components(separatedBy: kSeparator)
		let longer = max(lhsComponents.count, rhsComponents.count)
		(0 ..< longer - lhsComponents.count).forEach { _ in lhsComponents.append(kPaddingValue) }
		(0 ..< longer - rhsComponents.count).forEach { _ in rhsComponents.append(kPaddingValue) }
		let lhsVersion = lhsComponents.joined(separator: kSeparator)
		let rhsVersion = rhsComponents.joined(separator: kSeparator)

		let result = lhsVersion.compare(rhsVersion, options: .numeric)
		return result
	}

	/// Double value of the string, taking locale into account (decimal separator specifically).
	public func localizedDoubleValue(using locale: Locale = .current) -> Double? {
		guard let decimal = Decimal(string: self, locale: locale) else {
			return nil
		}
		return Double(truncating: decimal as NSNumber)
	}
}
