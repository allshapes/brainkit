//
//  NSAttributedString+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 15.10.2021.
//  Copyright © 2021 Userbrain. All rights reserved.
//

import Foundation

extension NSAttributedString {
	// originaly based on https://stackoverflow.com/a/54900313/1548913
	// fixed for multibyte encodings with https://sarunw.com/posts/how-to-remove-extra-padding-when-converting-html-to-nsattributedstring/#remove-new-line-from-nsattributedstring
	public func trimmingCharacters(in charSet: CharacterSet) -> NSAttributedString {
		let invertedSet = charSet.inverted
		let startRange = string.rangeOfCharacter(from: invertedSet)
		let endRange = string.rangeOfCharacter(from: invertedSet, options: .backwards)
		guard let startLocation = startRange?.lowerBound, let endLocation = endRange?.lowerBound else {
			return self
		}
		let range = NSRange(startLocation...endLocation, in: string)
		return attributedSubstring(from: range)
	}
}
