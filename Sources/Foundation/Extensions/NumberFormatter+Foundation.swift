//
//  NumberFormatter+Foundation.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 28.03.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import Foundation

extension NumberFormatter {
	public func string(from decimal: Decimal) -> String? {
		let dn = decimal as NSDecimalNumber
		let result = self.string(from: dn)
		return result
	}
}
