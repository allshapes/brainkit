//
//  DayNameDateFormatter.swift
//  iOS
//
//  Created by Ondřej Hanák on 07.03.2023.
//

import Foundation

public final class DayNameDateFormatter: DateFormatter {
	override public init() {
		super.init()
		dateFormat = "EEEE"
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
