//
//  SubscriptionPeriodFormatter.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 28.03.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import Foundation

public final class SubscriptionPeriodFormatter: DateComponentsFormatter {
	// MARK: - Lifecycle

	override public init() {
		super.init()
		self.maximumUnitCount = 1
		self.unitsStyle = .full
		self.zeroFormattingBehavior = .dropAll
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Public

	public func format(unit: NSCalendar.Unit, numberOfUnits: Int) -> String? {
		var dateComponents = DateComponents()
		dateComponents.calendar = Calendar.current
		self.allowedUnits = [unit]
		switch unit {
		case .day:
			dateComponents.setValue(numberOfUnits, for: .day)
		case .weekOfMonth:
			dateComponents.setValue(numberOfUnits, for: .weekOfMonth)
		case .month:
			dateComponents.setValue(numberOfUnits, for: .month)
		case .year:
			dateComponents.setValue(numberOfUnits, for: .year)
		default:
			return nil
		}

		return self.string(from: dateComponents)
	}
}
