//
//  File.swift
//  
//
//  Created by Ondřej Hanák on 26.04.2022.
//

import BrainKit
import Foundation
import SwiftyJSON

extension ObjectManager where T: JSONUpdatable {
	public func insert(from json: JSON) -> T {
		let object = self.insert()
		object.update(from: json)
		return object
	}
}
