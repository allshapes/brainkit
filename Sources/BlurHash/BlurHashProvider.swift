//
//  BlurHashProvider.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 24. 01. 2022.
//  Copyright © 2021 Userbrain. All rights reserved.
//

import UIKit

public protocol BlurHashProvider {
	var blurHash: String? { get }

	func blurHashImage(size: CGSize) -> UIImage?
}

extension BlurHashProvider {
	public func blurHashImage(size: CGSize = CGSize(width: 32, height: 32)) -> UIImage? {
		self.blurHash.flatMap { UIImage(blurHash: $0, size: size) }
	}
}
