//
//  UIApplication+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 30. 03. 2021.
//  Copyright © 2021 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension UIApplication {
	public var firstKeyWindow: UIWindow? {
		self.windows.first { $0.isKeyWindow }
	}

	@available(iOS 13.0, *)
	public var currentScene: UIWindowScene? {
		self.connectedScenes.first { $0.activationState == .foregroundActive } as? UIWindowScene
	}

	@available(iOS 13.0, *)
	public static var statusBarFrame: CGRect {
		let frame = UIApplication.shared.firstKeyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
		return frame
	}

	public static var interfaceOrientation: UIInterfaceOrientation {
		if #available(iOS 13, *) {
			let orinetation = UIApplication.shared.firstKeyWindow?.windowScene?.interfaceOrientation
			return orinetation ?? .unknown
		} else {
			return UIApplication.shared.statusBarOrientation
		}
	}

	public var topMostViewController: UIViewController? {
		UIApplication.shared.firstKeyWindow?.topMostViewController
	}

	public var topMostViewControllerBounds: CGRect {
		guard let topMostViewController = self.topMostViewController else { return UIScreen.main.bounds }
		return topMostViewController.view.bounds
	}

	public func openSettings() {
		let urlString: String
		#if targetEnvironment(macCatalyst)
			urlString = "x-apple.systempreferences:com.apple.preference.security"
		#else
			urlString = UIApplication.openSettingsURLString
		#endif
		guard let url = URL(string: urlString) else { return }
		self.open(url)
	}
}
#endif
