//
//  UILabel+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 06. 08. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension UILabel {
	public func setLineHeight(_ lineHeight: CGFloat) {
		guard let text = self.text else { return }
		let attributeString = NSMutableAttributedString(string: text)
		let style = NSMutableParagraphStyle()
		style.lineSpacing = lineHeight
		style.alignment = self.textAlignment
		attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: attributeString.length))
		self.attributedText = attributeString
	}

	public func setTextWithLineHeight(_ text: String?, _ lineHeight: CGFloat) {
		self.text = text
		guard let text = self.text else { return }
		let attributeString = NSMutableAttributedString(string: text)
		let style = NSMutableParagraphStyle()
		style.lineSpacing = lineHeight
		style.alignment = self.textAlignment
		attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: attributeString.length))
		self.attributedText = attributeString
	}

	public func setLineHeightMultiple(_ lineHeightMultiple: CGFloat) {
		guard let text = self.text else { return }
		let attributeString = NSMutableAttributedString(string: text)
		let style = NSMutableParagraphStyle()
		style.lineHeightMultiple = lineHeightMultiple
		style.alignment = self.textAlignment
		attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: attributeString.length))
		self.attributedText = attributeString
	}

	public func setTextWithLineHeightMultiple(_ text: String?, _ lineHeightMultiple: CGFloat) {
		self.text = text
		guard let text = self.text else { return }
		let attributeString = NSMutableAttributedString(string: text)
		let style = NSMutableParagraphStyle()
		style.lineHeightMultiple = lineHeightMultiple
		style.alignment = self.textAlignment
		attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: attributeString.length))
		self.attributedText = attributeString
	}
}
#endif
