//
//  UIButton+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 08.04.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension UIButton {
	public func setImageLabelSpacing(horizontal: CGFloat, vertical: CGFloat, between: CGFloat) {
		// based on https://stackoverflow.com/a/25559946/1548913
		let interInset = between / 2
		self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -interInset, bottom: 0, right: interInset)
		self.titleEdgeInsets = UIEdgeInsets(top: 0, left: interInset, bottom: 0, right: -interInset)
		self.contentEdgeInsets = UIEdgeInsets(top: vertical, left: interInset + horizontal, bottom: vertical, right: interInset + horizontal)
	}
}
#endif
