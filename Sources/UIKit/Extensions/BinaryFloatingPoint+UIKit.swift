//
//  BinaryFloatingPoint+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 14.02.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension BinaryFloatingPoint {
	public var squareSize: CGSize {
		CGSize(width: Double(self), height: Double(self))
	}
}
#endif
