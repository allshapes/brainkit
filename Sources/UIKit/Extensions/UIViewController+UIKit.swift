//
//  UIViewController+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 09. 04. 2021.
//  Copyright © 2021 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension UIViewController {
	public func setAvoidKeyboard(_ enabled: Bool) {
		let notificationCenter = NotificationCenter.default
		if enabled {
			notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
			notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
		} else {
			notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
			notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
		}
	}

	// MARK: - Private

	@objc
	private func adjustForKeyboard(notification: Notification) {
		guard let window = self.view.window else { return }
		guard let userInfo = notification.userInfo else { return }
		guard let keyboardValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
		guard let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else { return }
		guard let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt else { return }

		let keyboardScreenEndFrame = keyboardValue.cgRectValue
		let keyboardViewEndFrame = self.view.convert(keyboardScreenEndFrame, from: window)
		if keyboardViewEndFrame.width != keyboardScreenEndFrame.width && keyboardViewEndFrame.height != keyboardScreenEndFrame.height {
			// When VC is covered by another modal VC, which is dismissing, converted kb frame is flawed on first try, probably due to animation
			// On iPad 9th gen with iOS 15, frame is (-77.39759036144578, 773.9759036144577, 858.7951807228915, 339.27710843373495) instead of (-53.0, 718.0, 810.0, 320.0)
			// On iPhone 11 Pro with iOS 15, frame is (-17.492711370262388, 521.5014577259474, 409.9854227405247, 318.1486880466472) instead of (0.0, 467.0, 375.0, 291.0)
			// Tested on iOS 12, 14 and 15 physical phones + iOS 15 iPad simmulator
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
				self.adjustForKeyboard(notification: notification)
			}
			return
		}
		let lastBottomInset = self.view.safeAreaInsets.bottom
		var keyboardCoveredArea: CGFloat = 0
		if keyboardViewEndFrame.origin.y <= self.view.frame.size.height {
			keyboardCoveredArea = self.view.frame.size.height - keyboardViewEndFrame.origin.y
		}

		self.additionalSafeAreaInsets.bottom = 0 // always set to 0 to get unaltered safeAreaInsets

		if notification.name != UIResponder.keyboardWillHideNotification { // on hide keep additionalSafeAreaInsets on 0
			self.additionalSafeAreaInsets.bottom = keyboardCoveredArea - self.view.safeAreaInsets.bottom
		}

		guard abs(lastBottomInset - self.view.safeAreaInsets.bottom) > 45 else { return } // don't animate layout change on small difference. 45 is usually the height of the bar for autocomplete, and because it takes a moment to iOS to determine whether it's displayed or not, the adjustForKeyboard() fires twice. That causes visual jitter when navigating between fields and is not desired.
		guard lastBottomInset != self.view.safeAreaInsets.bottom else { return }

		UIView.animate(withDuration: duration, delay: 0, options: [UIView.AnimationOptions(rawValue: curve), .beginFromCurrentState], animations: {
			self.view.layoutIfNeeded()
		}, completion: nil)
	}

	public func addAsChild(_ child: UIViewController) {
		addChild(child)
		view.addSubview(child.view)
		child.didMove(toParent: self)
	}

	public func removeAsChild() {
		guard parent != nil else { return }
		willMove(toParent: nil)
		view.removeFromSuperview()
		removeFromParent()
	}
}
#endif
