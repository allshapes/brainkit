//
//  UIDevice+UIKit.swift
//  
//
//  Created by Ondřej Hanák on 15.09.2022.
//

#if os(iOS)
import UIKit

extension UIDevice {
	public var hasRoundedBezels: Bool {
		UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0
	}

	public var isSimulator: Bool {
	#if targetEnvironment(simulator)
		return true
	#else
		return false
	#endif
	}
}
#endif
