//
//  NSDirectionalEdgeInsets+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 23.08.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import UIKit

extension NSDirectionalEdgeInsets {
	public init(padding: CGFloat) {
		self.init(top: padding, leading: padding, bottom: padding, trailing: padding)
	}
}
