//
//  FixedWidthInteger+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 14.02.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension FixedWidthInteger {
	public var squareSize: CGSize {
		CGSize(width: Int(self), height: Int(self))
	}
}
#endif
