//
//  UIView+UIKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 03. 04. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

#if os(iOS)
import UIKit

extension UIView {
	public func asImage(scale: CGFloat = UIScreen.main.scale) -> UIImage {
		let format = UIGraphicsImageRendererFormat()
		format.scale = scale
		let renderer = UIGraphicsImageRenderer(bounds: bounds, format: format)
		return renderer.image { rendererContext in
			layer.render(in: rendererContext.cgContext)
		}
	}

	public func forAutolayout() -> Self {
		self.translatesAutoresizingMaskIntoConstraints = false
		return self
	}

	public func setShadow(withColor color: UIColor, opacity: Float, xOffset: Int, yOffset: Int, radius: CGFloat) {
		self.layer.shadowColor = color.cgColor
		self.layer.shadowOpacity = opacity
		self.layer.shadowOffset = CGSize(width: xOffset, height: yOffset)
		self.layer.shadowRadius = radius
	}

	public func setBorder(withColor color: UIColor, width: CGFloat) {
		self.layer.borderColor = color.cgColor
		self.layer.borderWidth = width
	}

	public func setCorners(withRadius radius: CGFloat, masked: CACornerMask? = nil) {
		self.layer.cornerRadius = radius
		if let masked = masked {
			self.layer.maskedCorners = masked
		}
	}

	@available(iOS 13, *)
	public func setCorners(withRadius radius: CGFloat, masked: CACornerMask? = nil, curve: CALayerCornerCurve = .continuous) {
		self.setCorners(withRadius: radius, masked: masked)
		self.layer.cornerCurve = curve
	}

	public func firstParent<T>(ofType type: T.Type) -> T? {
		if let s = superview as? T {
			return s
		} else {
			return superview?.firstParent(ofType: type)
		}
	}
}
#endif
