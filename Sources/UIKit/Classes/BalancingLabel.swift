//
//  BalancingLabel.swift
//  iOS
//
//  Created by Ondřej Hanák on 22.11.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

// Based on https://github.com/Julioacarrettoni/DEVJACAutoBalancedLabel

#if os(iOS)
import UIKit

public class BalancingLabel: UILabel {
	public override func drawText(in rect: CGRect) {
		var newRect = rect
		if textAlignment == .center {
			let oneLineRect = textRect(forBounds: .infinite, limitedToNumberOfLines: 1)
			let numberOfLines = ceil(oneLineRect.size.width / bounds.size.width)
			var betterWidth = oneLineRect.size.width / numberOfLines
			if betterWidth < rect.size.width {
				var check = CGRect.zero
				repeat {
					betterWidth *= 1.1
					let b = CGRect(x: 0, y: 0, width: betterWidth, height: CGRect.infinite.size.height)
					check = textRect(forBounds: b, limitedToNumberOfLines: 0)
				} while check.size.height > rect.size.height && betterWidth < rect.size.width
				if betterWidth < rect.size.width {
					let difference = rect.size.width - betterWidth
					newRect = CGRect(x: rect.origin.x + difference / 2, y: rect.origin.y, width: betterWidth, height: rect.size.height)
				}
			}
		}
		super.drawText(in: newRect)
	}
}
#endif
