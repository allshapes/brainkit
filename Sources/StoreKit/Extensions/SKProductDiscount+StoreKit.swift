//
//  SKProductDiscount+StoreKit.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 09.11.2021.
//  Copyright © 2021 Userbrain. All rights reserved.
//

// based on https://stackoverflow.com/a/56870047/1548913

import StoreKit

@available(iOS 11.2, watchOS 6.2, *)
extension SKProductDiscount {
	public func localizedPeriod() -> String? {
		switch self.paymentMode {
		case .freeTrial:
			return SubscriptionPeriodFormatter().format(unit: self.subscriptionPeriod.unit.toCalendarUnit(), numberOfUnits: self.subscriptionPeriod.numberOfUnits)
		case .payUpFront, .payAsYouGo:
			return SubscriptionPeriodFormatter().format(unit: self.subscriptionPeriod.unit.toCalendarUnit(), numberOfUnits: self.numberOfPeriods)
		@unknown default:
			return nil
		}
	}
}
