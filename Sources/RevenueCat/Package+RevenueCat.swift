//
//  Package+RevenueCat.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 11.04.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import RevenueCat

@available(iOS 11.2, *)
extension RevenueCat.Package {
	public func formattedIntroductoryPrice() -> String {
		if let intro = self.storeProduct.introductoryDiscount, self.storeProduct.hasIntroPrice {
			return self.storeProduct.priceFormatter?.string(from: intro.price) ?? ""
		}
		return self.formattedRegularPrice()
	}

	public func formattedRegularPrice() -> String {
		self.storeProduct.priceFormatter?.string(from: self.storeProduct.price) ?? ""
	}

	public func localizedIntroductoryDuration() -> String? {
		return self.storeProduct.introductoryDiscount?.localizedPeriod()
	}
}
