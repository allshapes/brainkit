//
//  StoreProductDiscount+RevenueCat.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 11.04.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import BrainKit
import RevenueCat

extension RevenueCat.StoreProductDiscount {
	public func localizedPeriod() -> String? {
		switch self.paymentMode {
		case .freeTrial:
			return SubscriptionPeriodFormatter().format(unit: self.subscriptionPeriod.unit.calendarUnit, numberOfUnits: self.subscriptionPeriod.value)
		case .payUpFront, .payAsYouGo:
			return SubscriptionPeriodFormatter().format(unit: self.subscriptionPeriod.unit.calendarUnit, numberOfUnits: self.numberOfPeriods)
		}
	}
}
