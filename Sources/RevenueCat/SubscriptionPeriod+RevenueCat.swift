//
//  SubscriptionPeriod+RevenueCat.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 11.04.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import RevenueCat
import UIKit

extension RevenueCat.SubscriptionPeriod.Unit {
	public var calendarUnit: NSCalendar.Unit {
		switch self {
		case .day:
			return .day
		case .week:
			return .weekOfMonth
		case .month:
			return .month
		case .year:
			return .year
		}
	}
}
