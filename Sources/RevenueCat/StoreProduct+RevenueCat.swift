//
//  StoreProduct+RevenueCat.swift
//  BrainKit
//
//  Created by Ondřej Hanák on 11.04.2022.
//  Copyright © 2022 Userbrain. All rights reserved.
//

import RevenueCat

@available(iOS 11.2, *)
extension RevenueCat.StoreProduct {
	public var hasFreeTrial: Bool {
		self.introductoryDiscount?.type == .introductory && self.introductoryDiscount?.paymentMode == .freeTrial
	}

	public var hasIntroPrice: Bool {
		self.introductoryDiscount?.type == .introductory && self.introductoryDiscount?.paymentMode != .freeTrial
	}
}
