//
//  TestHelpers.swift
//  
//
//  Created by Ondřej Hanák on 10.12.2021.
//

import Foundation
import CoreData
import Foundation
import BrainKit
import BrainKitSwiftyJSON
import XCTest
import SwiftyJSON

// MARK: - CoreData support

@objc(RelatedEntity)
class RelatedEntity: NSManagedObject {
	@NSManaged var testing: TestingEntity?
}

@objc(TestingEntity)
class TestingEntity: NSManagedObject {
	@NSManaged var id: Int64
	@NSManaged var ord: Int32
	@NSManaged var related: Set<RelatedEntity>
}

extension TestingEntity: OrderableManagedObject {}

extension TestingEntity: IdentifiedManagedObject {}

extension TestingEntity: JSONUpdatable {
	func update(from json: JSON) {
		self.id = json["id"].int64Value
	}
}

// MARK: - TextCase support

class TestCoreDataTestCase: XCTestCase {
	var coreDataContainer: CoreDataContainer!

	override func setUp() {
		super.setUp()
		self.coreDataContainer = CoreDataContainer(modelName: "Testing", bundle: self.testBundle, storageType: .inMemory)
	}

	override func tearDown() {
		self.coreDataContainer = nil
		super.tearDown()
	}
}

extension XCTestCase {
	var testBundle: Bundle {
#if SWIFT_PACKAGE
		Bundle.module
#else
		Bundle(for: type(of: self))
#endif
	}
}

// based on https://stackoverflow.com/a/44140448/1548913

extension XCTestCase {
	func expectFatalError(expectedMessage: String, testcase: @escaping () -> Void) {

		// arrange
		let expectation = self.expectation(description: "expectingFatalError")
		var assertionMessage: String? = nil

		// override fatalError. This will pause forever when fatalError is called.
		FatalErrorUtil.replaceFatalError { message, _, _ in
			assertionMessage = message
			expectation.fulfill()
			unreachable()
		}

		// act, perform on separate thead because a call to fatalError pauses forever
		DispatchQueue.global(qos: .userInitiated).async(execute: testcase)

		waitForExpectations(timeout: 0.1) { _ in
			// assert
			XCTAssertEqual(assertionMessage, expectedMessage)

			// clean up
			FatalErrorUtil.restoreFatalError()
		}
	}
}
