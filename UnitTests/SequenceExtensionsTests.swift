//
//  SequenceExtensionsTests.swift
//  UnitTests
//
//  Created by Ondřej Hanák on 04. 03. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import Foundation
import BrainKit
import XCTest

final class SequenceExtensionsTests: XCTestCase {
	func test_Unique() {
		let input = [1,2,3,3,2,1,4]
		let output = input.unique()
		XCTAssertEqual(output, [1,2,3,4])
	}

	func test_Sorting_Ascending() {
		let input = [1, 10, 2, 20, 3, 30]
		let sorted = input.sorted(by: \.self)
		XCTAssertEqual([1, 2, 3, 10, 20, 30], sorted)
	}

	func test_Sorting_Descending() {
		let input = [1, 10, 2, 20, 3, 30]
		let sorted = input.sorted(by: \.self, ascending: false)
		XCTAssertEqual([30, 20, 10, 3, 2, 1], sorted)
	}
}
