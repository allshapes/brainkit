//
//  UIViewExtensionsTests.swift
//  UnitTests
//
//  Created by Ondřej Hanák on 20. 05. 2020.
//  Copyright © 2020 Userbrain. All rights reserved.
//

import XCTest
import BrainKit

final class UIViewExtensionsTests: XCTestCase {
	func test_ForAutolayout() {
		let auto = UIImageView().forAutolayout()
		XCTAssertFalse(auto.translatesAutoresizingMaskIntoConstraints)
	}

	func test_firstParentOfType_noParent() {
		let sut = UIView()
		let result = sut.firstParent(ofType: UILabel.self)
		XCTAssertNil(result)
	}

	func test_firstParentOfType_parentOfWrongClass() {
		let sut = UIView()
		let parent = UIView()
		parent.addSubview(sut)
		let result = sut.firstParent(ofType: UILabel.self)
		XCTAssertNil(result)
	}

	func test_firstParentOfType_parentOfCorrectClass() {
		let sut = UIView()
		let parent = UILabel()
		parent.addSubview(sut)
		let result = sut.firstParent(ofType: UILabel.self)
		XCTAssertEqual(parent, result)
	}
}
