//
//  UIViewControllerTests.swift
//
//
//  Created by Ondřej Hanák on 28.02.2024.
//

import Foundation
import BrainKit
import XCTest

final class UIViewControllerTests: XCTestCase {
	func test_addAndRemoveAsChild() {
		let parent = UIViewController()
		let child = UIViewController()

		XCTAssertNil(child.view.superview)
		XCTAssertNil(child.parent)
		
		parent.addAsChild(child)

		XCTAssertEqual(child.view.superview, parent.view)
		XCTAssertEqual(child.parent, parent)

		child.removeAsChild()

		XCTAssertNil(child.view.superview)
		XCTAssertNil(child.parent)
	}
}

