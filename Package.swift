// swift-tools-version:5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "BrainKit",
	platforms: [
		.iOS(.v13),
		.watchOS("6.2"),
		.macOS(.v10_15),
	],
	products: [
		.library(name: "BrainKit", targets: ["BrainKit"]),
		.library(name: "BrainKitBlurHash", targets: ["BrainKitBlurHash"]),
		.library(name: "BrainKitSwiftyJSON", targets: ["BrainKitSwiftyJSON"]),
		.library(name: "BrainKitSnapKit", targets: ["BrainKitSnapKit"]),
		.library(name: "BrainKitRevenueCat", targets: ["BrainKitRevenueCat"]),
	],
	dependencies: [
		.package(url: "https://github.com/SwiftyJSON/SwiftyJSON", from: "5.0.0"),
		.package(url: "https://github.com/SnapKit/SnapKit", from: "5.0.0"),
		.package(url: "https://github.com/RevenueCat/purchases-ios-spm", from: "5.0.0"),
	],
	targets: [
		.target(
			name: "BrainKit",
			path: "Sources",
			exclude: ["BlurHash", "SwiftyJSON", "SnapKit", "RevenueCat"],
			sources: ["AVFoundation", "CoreData", "Foundation", "SafariServices", "StoreKit", "UIKit", "WebKit"]
		),
		.target(
			name: "BrainKitBlurHash",
			path: "Sources/BlurHash",
			exclude: ["Readme.md"]
		),
		.target(
			name: "BrainKitSwiftyJSON",
			dependencies: ["BrainKit", "SwiftyJSON"],
			path: "Sources/SwiftyJSON"
		),
		.target(
			name: "BrainKitSnapKit",
			dependencies: ["SnapKit"],
			path: "Sources/SnapKit"
		),
		.target(
			name: "BrainKitRevenueCat",
			dependencies: ["BrainKit", .product(name: "RevenueCat", package: "purchases-ios-spm")],
			path: "Sources/RevenueCat"
		),
		.testTarget(
			name: "UnitTests",
			dependencies: ["BrainKit", "BrainKitBlurHash", "BrainKitSwiftyJSON", "BrainKitSnapKit", "BrainKitRevenueCat"],
			path: "UnitTests",
			exclude: ["Info.plist"],
			resources: [
				.copy("Resources/HelloWorld.txt"),
				.copy("Resources/HelloWorld.plist"),
				.copy("Resources/HelloWorld.json"),
				.copy("Resources/File.bin"),
				.process("Resources/Testing.xcdatamodeld"),
				.process("Resources/StoryboardViewController.storyboard"),
			]
		),
	]
)
