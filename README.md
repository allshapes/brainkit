# BrainKit

## Installation via SPM

* Add package as https://bitbucket.org/allshapes/brainkit


## Installation via CocoaPods

* Add `brainpods` repository to the local CocoaPods instalation

```
pod repo add brainpods https://bitbucket.org/allshapes/brainpods.git
```

* Add explicit sources to the Podfile

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/allshapes/brainpods.git'
```

* Add BrainKit to the Podfile

```
pod 'BrainKit'
```


## Creation of new version

- Commit your changes
- Validate locally by runing `./lint-local`
- Update version in `podspec`
- Commit with description of `Release x.y.z`
- Add version tag like `x.y.z`
- Push changes along with the new tag
- Update pod repository by runing `./update-brainpods`
